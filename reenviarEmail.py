import requests
import json
import csv
import time
import jwt
from pathlib import Path

"""
Get an access token using OAuth2 client credentials.
"""
def get_access_token(service_host, client_id, client_secret):
    print(f"Autenticando...")
    token_url = 'https://'+service_host+'/token-service/jwt'
    payload = {
        "grant_type": "client_credentials",
        "client_id": client_id,
        "client_secret": client_secret,
    }
    response = requests.post(token_url, data=payload)
    if response.status_code == 200:
        access_token = response.json()["access_token"]
        print(f"Autenticado!")
        return access_token
    else:
        print(f"Falha ao autenticar na API: {response.content}")
        return None

"""
Resend emails for a given workflow key using an OAuth2 access token.
"""
def resendEmail(service_host, access_token, wkey, cpf):
    print(f"Buscando coleta {wkey}...")
    headers = {
        "Authorization": f"Bearer {access_token}",
        "Content-Type": "application/json",
    }

    participantes = {
        "participantes": [cpf]
    }
    
    response = requests.post('https://'+service_host+'/scad/rest/coletas/'+wkey+'/reenviar-email', headers=headers, params=None, json=participantes)
    if response.status_code == 200:
        print('Email reenviado para '+cpf)
        return True
    else:
        print(f"Falha ao reenviar email: {response.content}")
        return False



def main(csv_file, config_file):
    with open(config_file, "r") as f:
        config = json.load(f)

    service_host = config["service_host"]
    access_token = get_access_token(service_host, config["client_id"], config["client_secret"])

    processed_rows = [];
    with open(csv_file, newline="", encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['resent'] != 'True':
                wkey=row['wkey']
                cpf=row['cpf']
                decoded_token = jwt.decode(access_token, options={"verify_signature": False}, algorithms=["RS256"])
                """print("Validade token: "+str(decoded_token['exp'])+" agora: "+str(int(time.time())))"""
                if decoded_token['exp'] < int(time.time()):
                    access_token = get_access_token(service_host, config["client_id"], config["client_secret"])
                if resendEmail(service_host, access_token, wkey, cpf):
                    row['resent'] = 'True'
                else:
                    row['resent'] = 'False'
            processed_rows.append(row);
    fieldnames = list(processed_rows[0].keys())
    with open(csv_file, 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(processed_rows)
    print('Reenvio finalizado')

if __name__ == "__main__":
    import sys

    if len(sys.argv) < 2:
        print("Usage: python download.py [csv_file]")
        sys.exit(1)

    csv_file = sys.argv[1]
    config_file = "config.json"
    try:
        main(csv_file, config_file)
    except Exception as e:
        print(f"An error occurred: {str(e)}")
        sys.exit(1)