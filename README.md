# Reenvio de Emails

## Introdução

Este exemplo permite o reenvio de emails para coletas de assinatura existentes na Assinatura em Grupo. Uma planilha (CSV) é utilizada como entrada e controle dos reenvios realizados, podendo cada linha receber uma coleta e participante que deve ser notificado. 

## Como utilizar

- Faça o download dos arquivos reenviarEmail.py, config.json e teste_resend.csv
- Edite o arquivo config.json, ajustando as configurações do client_id e client_secret da sua aplicação cadastrada no BRy Cloud.
- Edite o arquivo teste_resend.csv, incluíndo a chave da coleta e o identificador (CPF) de cada participante que deve ser notificado. 

- Execute:  

```
python3 reenviarEmail.py teste_resend.csv
```

## Formato de entrada

A planilha deve ter uma linha por coleta e participante a ser notificado. 

- wkey: chave da coleta no BRy Cloud;
- cpf: CPF do participante a ser notificado;
- resent: Deve iniciar como "False". Vai ser alterado para "True" caso o reenvio ocorra com sucesso;

## Formato de saída 

A linha "resent" da planilha será alterada para "True" caso a notificação ocorra com sucesso. Dessa forma, você pode sempre usar a mesma planilha para continuar notificações de coletas ainda pendentes, pois as linhas onde "resent" for "True" não serão consultadas novamente em próximas execuções. 
